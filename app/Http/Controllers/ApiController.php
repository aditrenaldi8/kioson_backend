<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Input;

class ApiController extends Controller
{
    function getKota(Request $request){

        /*
            get data request post dengan from body sbg berikut:
            ex:
                {
                    "propinsi" : "BALI"
                }

            *syarat header -> content-type : application/json

            mengganti hasil data yang didapat dari request option, karakter spasi
            (" ") diganti dengan karakter ("%20"), hal ini disesuaikan dengan requirement 
            API dari kioson.com/carimitra

        */ 

    	$propinsi_= str_replace(" ","%20",$request->propinsi);

        /*
            menggunakan library guzzlehttp, function request($method, $uri = ''); 
        */

        $client = new Client();
        $res = $client->request('GET', 'https://www.kioson.com/carimitra/getkota?propinsi='. $propinsi_ );

        /* 
            mengembalikan nilai yang didapat dari hasil request
        */
        
        return $res->getBody();
    }

    function getKecamatan(Request $request){

        /*  
            get data request post dengan from body sbg berikut:
            ex:
                {
                    "propinsi" : "BALI",
                    "kota" : "BADUNG"
                }

            *syarat header -> content-type : application/json

            mengganti hasil data yang didapat dari request option, karakter spasi
            (" ") diganti dengan karakter ("%20"), hal ini disesuaikan dengan requirement 
            API dari kioson.com/carimitra

        */

    	$propinsi_= str_replace(" ","%20",$request->propinsi);
        $kota_= str_replace(" ","%20",$request->kota);

        /*
            menggunakan library guzzlehttp, function request($method, $uri = ''); 
        */

        $client = new Client();
        $res = $client->request('GET', 'https://www.kioson.com/carimitra/getkecamatan?propinsi='. $propinsi_ .'&kota='.$kota_);


        /* 
            mengembalikan nilai yang didapat dari hasil request
        */
        
        return $res->getBody();
    }

    function getKelurahan(Request $request){

        /*
            get data request post dengan from body sbg berikut:
            ex:
                {
                    "propinsi" : "BALI",
                    "kota" : "BADUNG",
                    "kecamatan" : "KUTA UTARA"
                }

            *syarat header -> content-type : application/json

            mengganti hasil data yang didapat dari request option, karakter spasi
            (" ") diganti dengan karakter ("%20"), hal ini disesuaikan dengan requirement 
            API dari kioson.com/carimitra

        */

    	$propinsi_= str_replace(" ","%20",$request->propinsi);
    	$kota_= str_replace(" ","%20",$request->kota);
    	$kec_ = str_replace(" ","%20",$request->kecamatan);

        /*
            menggunakan library guzzlehttp, function request($method, $uri = '');
        */

        $client = new Client();
        $res = $client->request('GET', 'https://www.kioson.com/carimitra/getkelurahan?propinsi='. $propinsi_ .'&kota='.$kota_ .'&kecamatan=' . $kec_);
        
        /* 
            mengembalikan nilai yang didapat dari hasil request
        */

        return $res->getBody();
    }

    function getKios(Request $request){

        /*
            get data request post dengan from body sbg berikut:
            ex:
                {
                    "propinsi" : "BALI",
                    "kota" : "BADUNG",
                    "kecamatan" : "KUTA UTARA",
                    "kelurahan" : "DALUNG"
                }

            dan dimasukkan ke dalam array, untuk dikirimkan sebagai parameter 
            form data yang dibutuhkan api dari url https://www.kioson.com/carimitra/tokojson

        */

        $data = array(
            'propinsi'=> $request->propinsi,
            'kota' => $request->kota,
            'kecamatan' => $request->kecamatan,
            'kelurahan' => $request->kelurahan
        );  


        /*
            menggunakan library guzzlehttp,
            function request($method, $uri = '', array $options = []);
        */

	    $client = new Client();
	    $res = $client->request('POST', 'https://www.kioson.com/carimitra/tokojson', [
	        'form_params' => $data
	    ]);

	    $data = json_decode($result= $res->getBody());

        /*
            mengecek apakah hasil result null atau tidak
        */

        if(isset($data)){

            /*
                mengubah tag <br />, yang merupakan hasil yg didapat
                menjadi " , " (koma)
            */

            foreach($data as $a){
                $a->alamat = str_replace("<br />",", ", $a->alamat);
            }
        }

        /* 
            mengembalikan nilai yang didapat dari hasil request
        */

        return response()->json($data);
	    
	}

    function savePhone(Request $request){

        /*
            save data email, syarat content-type : application/json
            body
            ex: 
                {
                    "phone_number" : "081123456789",
                }
        */

        DB::beginTransaction();

        try{
            $this->validate($request,[
                'phone_number' => 'required',
            ]);

            DB::table('phones')->insert(
                [
                    'phone_number'=> $request->phone_number,
                    'created_at' => date("Y-m-d h:i:s")
                ]
            ); 
            
            DB::commit();  
            return json_encode(array('status' => 200, 'message' => "berhasil disimpan"));

        }catch(\Exception $e){
            DB::rollBack();
            return response()->json(["message"=> $e->getMessage ], 500);
        }
    }

    function saveEmail(Request $request){

        /*
            save data email, syarat content-type : application/json
            body
            ex: 
                {
                    "name" : "xxx",
                    "email" : "xxx@xx.com"
                }

        */

        DB::beginTransaction();

        try{

            $this->validate($request,[
                'name' => 'required',
                'email' => 'required|email',
            ]);
  
            DB::table('users')->insert(
                [
                    'name'=> $request->name,
                    'email'=> $request->email,
                    'created_at'=> date("Y-m-d h:i:s")
                ]
            );   

            DB::commit(); 
            return json_encode(array('status' => 200, 'message' => "berhasil disimpan"));
        }catch(\Exception $e){
            DB::rollBack();
            return response()->json(["message"=> $e->getMessage ], 500);
        }
    }
}
